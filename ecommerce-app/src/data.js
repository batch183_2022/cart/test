import brand1 from "./img/brand1.png";
import brand2 from "./img/brand2.png";
import brand3 from "./img/brand3.png";

const data = {
	productData:[
		{
			id: 1,
			img: brand1,
			title: "Nokia Phone",
			desc: "",
			price: 46
		},
		{
			id: 2,
			img: brand2,
			title: "Printer Canon",
			desc: "",
			price: 56
		},
		{
			id: 3,
			img: brand3,
			title: "Samsung Phone",
			desc: "",
			price: 46
		},

	]
}

export default data;